package com.lunchbot.app;

import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.lunchbot.app.menu.UlemisteCityMenu;
import com.lunchbot.app.module.PropertyModule;
import com.lunchbot.app.module.SlackModule;
import com.lunchbot.app.service.PropertyService;
import com.lunchbot.app.service.SlackService;

public class Application {
    private final SlackService slackService;

    @Inject
    public Application(SlackService slackService) {
        this.slackService = slackService;
    }

    public static void main(String[] args) {
        try {
            Injector propertyServiceInjector = Guice.createInjector(new PropertyModule());
            PropertyService propertyService = propertyServiceInjector.getInstance(PropertyService.class);

            Injector slackServiceInjector = Guice.createInjector(new SlackModule());
            SlackService slackService = slackServiceInjector.getInstance(SlackService.class);

            UlemisteCityMenu menu = new UlemisteCityMenu(
                propertyService.getProperty("menu.url"),
                propertyService.getProperty("menu.image")
            );

            slackService.sendWebhook(
                propertyService.getProperty("slack.webhook.url"),
                slackService.composeMessage(menu.getMenu())
            );
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}
