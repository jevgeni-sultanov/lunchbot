package com.lunchbot.app.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Collection;

@Data
@Accessors(chain = true)
public class SlackMessageDto {
    Collection<?> blocks;

    public static class Section implements SlackObjectInterface {
        public String type = "section";
        public MarkDown text;

        public Section(MarkDown markDown) {
            this.text = markDown;
        }
    }

    @Data
    @Accessors(chain = true)
    public static class Context implements SlackObjectInterface {
        public String type = "context";
        public Collection<?> elements;
    }

    public static class MarkDown implements SlackObjectInterface {
        public String type = "mrkdwn";
        public String text;

        public MarkDown(String text) {
            this.text = text;
        }
    }

    public static class Image implements SlackObjectInterface {
        public String type = "image";
        public String image_url;
        public String alt_text;

        public Image(String image_url, String alt_text) {
            this.image_url = image_url;
            this.alt_text = alt_text;
        }
    }

    public static class Divider implements SlackObjectInterface {
        public String type = "divider";
    }

    @Data
    @Accessors(chain = true)
    public static class Action implements SlackObjectInterface {
        public String type = "actions";
        public Collection<?> elements;
    }

    public static class Button implements SlackObjectInterface {
        public String type = "button";
        public String style = "danger";
        public PlainText text;
        public String url;

        public Button(PlainText plainText, String url) {
            this.text = plainText;
            this.url = url;
        }
    }

    public static class PlainText implements SlackObjectInterface {
        public String type = "plain_text";
        public Boolean emoji = true;
        public String text;

        public PlainText(String text) {
            this.text = text;
        }
    }
}
