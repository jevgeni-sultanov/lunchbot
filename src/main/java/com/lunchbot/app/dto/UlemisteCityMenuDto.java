package com.lunchbot.app.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class UlemisteCityMenuDto {
    private String menuImageUrl;
    public List<UlemisteCityRestaurantDto> restaurantDtoList;
}
