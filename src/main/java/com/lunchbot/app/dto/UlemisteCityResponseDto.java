package com.lunchbot.app.dto;

import lombok.Data;

import java.util.List;

@Data
public class UlemisteCityResponseDto {
    public Boolean success;
    public MenuData data;

    @Data
    public static class MenuData {
        public List<?> filters;
        public List<UlemisteCityRestaurantDto> entries;
    }
}
