package com.lunchbot.app.dto;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class UlemisteCityRestaurantDto {
    public String title;
    public String permalink;
    public String location;
    public List<Offer> offers;

    @Data
    public static class Offer {
        public String name;
        public String price;
    }
}
