package com.lunchbot.app.menu;

import com.google.gson.Gson;
import com.lunchbot.app.dto.UlemisteCityMenuDto;
import com.lunchbot.app.dto.UlemisteCityResponseDto;
import com.lunchbot.app.dto.UlemisteCityRestaurantDto;
import org.jsoup.Jsoup;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class UlemisteCityMenu {
    private final String menuUrl;

    private final String imageUrl;

    private static final String EURO_SYMBOL = "\u20ac";

    public UlemisteCityMenu(String menuUrl, String imageUrl) {
        this.menuUrl = menuUrl;
        this.imageUrl = imageUrl;
    }

    /**
     * Parses UlemisteCity site menu
     *
     * @return Map
     * @throws IOException
     * @throws InterruptedException
     */
    public UlemisteCityMenuDto getMenu() throws IOException, InterruptedException {
        HttpClient client = HttpClient.newBuilder().build();

        HttpRequest request = HttpRequest.newBuilder(URI.create(this.menuUrl))
            .header("Content-Type", "application/x-www-form-urlencoded; charset=utf-8")
            .POST(
                HttpRequest
                    .BodyPublishers
                    .ofString("what=daily-offers&time=" + Instant.now().getEpochSecond() + "&lang=en&action=ld_get_content")
            )
            .build();

        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
        UlemisteCityResponseDto responseDto = new Gson().fromJson(response.body(), UlemisteCityResponseDto.class);

        List<UlemisteCityRestaurantDto> restaurantDtoList = new ArrayList<>();

        responseDto.getData().getEntries().forEach(restaurant -> {
            UlemisteCityRestaurantDto restaurantDto = new UlemisteCityRestaurantDto();
            restaurantDto.setTitle(restaurant.getTitle());
            restaurantDto.setPermalink(restaurant.getPermalink());
            restaurantDto.setLocation(restaurant.getLocation());

            if (!restaurant.getOffers().isEmpty()) {
                List<UlemisteCityRestaurantDto.Offer> restaurantOffers = new ArrayList<>();

                restaurant.getOffers().forEach(offer -> {
                    String courseTitle = Jsoup.parse(offer.getName()).text().trim();
                    if (!courseTitle.isEmpty() && offer.getPrice() != null) {
                        restaurantOffers.add(
                            new UlemisteCityRestaurantDto.Offer()
                                .setName(courseTitle)
                                .setPrice(offer.getPrice().replace(EURO_SYMBOL, "").trim() + " " + EURO_SYMBOL)
                        );
                    }
                });

                if (!restaurantOffers.isEmpty()) {
                    restaurantDto.setOffers(restaurantOffers);
                    restaurantDtoList.add(restaurantDto);
                }
            }
        });

        return new UlemisteCityMenuDto()
            .setMenuImageUrl(this.getRandomLunchImage())
            .setRestaurantDtoList(restaurantDtoList);
    }

    /**
     * Get a random image url from a web service
     *
     * @return String
     * @throws IOException
     * @throws InterruptedException
     */
    private String getRandomLunchImage() throws IOException, InterruptedException {
        HttpClient client = HttpClient.newBuilder()
                .followRedirects(HttpClient.Redirect.NORMAL)
                .connectTimeout(Duration.ofSeconds(20))
                .build();

        HttpRequest request = HttpRequest.newBuilder(URI.create(this.imageUrl))
                .header("Content-Type", "application/x-www-form-urlencoded; charset=utf-8")
                .GET()
                .build();

        return client.send(request, HttpResponse.BodyHandlers.ofString()).uri().toString();
    }
}
