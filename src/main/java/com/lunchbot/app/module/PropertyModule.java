package com.lunchbot.app.module;

import com.google.inject.AbstractModule;
import com.lunchbot.app.service.PropertyService;
import com.lunchbot.app.service.PropertyServiceInterface;

public class PropertyModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(PropertyServiceInterface.class).to(PropertyService.class);
    }
}
