package com.lunchbot.app.module;

import com.google.inject.AbstractModule;
import com.lunchbot.app.service.SlackService;
import com.lunchbot.app.service.SlackServiceInterface;

public class SlackModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(SlackServiceInterface.class).to(SlackService.class);
    }
}
