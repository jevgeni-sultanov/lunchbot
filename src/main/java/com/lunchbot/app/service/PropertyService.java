package com.lunchbot.app.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyService implements PropertyServiceInterface {
    private final Properties properties;

    public PropertyService() throws IOException {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("config.properties");
        Properties properties = new Properties();

        if (inputStream != null) {
            properties.load(inputStream);
        } else {
            throw new FileNotFoundException("Property file not found in the classpath");
        }

        this.properties = properties;
    }

    public String getProperty(String propertyName) {
        return this.properties.getProperty(propertyName);
    }
}
