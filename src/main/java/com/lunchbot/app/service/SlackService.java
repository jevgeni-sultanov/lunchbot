package com.lunchbot.app.service;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.lunchbot.app.dto.SlackMessageDto;
import com.lunchbot.app.dto.SlackObjectInterface;
import com.lunchbot.app.dto.UlemisteCityMenuDto;
import com.lunchbot.app.dto.UlemisteCityRestaurantDto;
import org.jsoup.Jsoup;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

public class SlackService implements SlackServiceInterface {
    private final PropertyService propertyService;

    @Inject
    public SlackService(PropertyService propertyService) {
        this.propertyService = propertyService;
    }

    /**
     * Generates a message to the Slack service
     *
     * @param menuDto Restaurant titles and courses
     * @return JSON String
     */
    public String composeMessage(UlemisteCityMenuDto menuDto) {
        Collection<SlackObjectInterface> collection = new ArrayList<>();
        collection.add(this.createMessageImage(menuDto.getMenuImageUrl(), "LUNCH"));

        menuDto.getRestaurantDtoList().forEach(restaurantDto -> {
            collection.add(this.createMessageSection(restaurantDto));
            collection.add(this.createMessageContext(restaurantDto.getOffers()));
        });

        collection.add(this.createMessageButton(this.propertyService.getProperty("slack.lunch.button.url")));

        return new Gson().toJson(new SlackMessageDto().setBlocks(collection));
    }

    /**
     * Sends an authenticated request to a provided URL
     *
     * @param url URL address
     * @param message Message body
     * @return HttpResponse
     * @throws IOException
     * @throws InterruptedException
     */
    public HttpResponse<String> sendWebhook(String url, String message) throws IOException, InterruptedException {
        HttpClient client = HttpClient.newBuilder().build();

        HttpRequest request = HttpRequest.newBuilder(URI.create(url))
            .header("Content-Type", "application/json; charset=utf-8")
            .header("Authorization", "Bearer " + this.propertyService.getProperty("slack.bearer.token"))
            .POST(HttpRequest.BodyPublishers.ofString(message))
            .build();

        return client.send(request, HttpResponse.BodyHandlers.ofString());
    }

    /**
     * Creates Slack image
     *
     * @param url The lunch image url
     * @param altText The lunch image alternative text
     * @return SlackObject
     */
    private SlackObjectInterface createMessageImage(String url, String altText) {
        return new SlackMessageDto.Image(url, altText);
    }

    /**
     * Creates Slack section
     *
     * @param restaurantDto The restaurant name
     * @return SlackObject
     */
    private SlackObjectInterface createMessageSection(UlemisteCityRestaurantDto restaurantDto) {
        StringBuilder section = new StringBuilder();
        section.append(this.getRandomEmoji())
            .append(" *")
            .append(restaurantDto.getTitle().toUpperCase())
            .append(":*")
            .append(System.lineSeparator())
            .append('_')
            .append(Jsoup.parse(restaurantDto.getLocation()).text().trim())
            .append('_')
            .append(System.lineSeparator())
            .append(Jsoup.parse(restaurantDto.getPermalink()).text().trim())
            .append(System.lineSeparator());

        return new SlackMessageDto.Section(
            new SlackMessageDto.MarkDown(
                section.toString()
            )
        );
    }

    /**
     * Creates Slack context with elements
     *
     * @param courses The restaurant courses map
     * @return SlackObject
     */
    private SlackObjectInterface createMessageContext(List<UlemisteCityRestaurantDto.Offer> courses) {
        Collection<SlackObjectInterface> contextElements = new ArrayList<>();
        StringBuilder courseString = new StringBuilder();

        courses.forEach(course -> {
            courseString.append(course.getName())
                .append(" : *")
                .append(course.getPrice())
                .append("*")
                .append(System.lineSeparator());
        });

        contextElements.add(new SlackMessageDto.MarkDown(courseString.toString()));

        return new SlackMessageDto.Context().setElements(contextElements);
    }

    /**
     * Creates Slack button
     *
     * @return SlackObject
     */
    private SlackObjectInterface createMessageButton(String action) {
        Collection<SlackObjectInterface> actionElements = new ArrayList<>();
        actionElements.add(
            new SlackMessageDto.Button(
                new SlackMessageDto.PlainText(this.propertyService.getProperty("slack.lunch.button.text")),
                action
            )
        );

        return new SlackMessageDto.Action().setElements(actionElements);
    }

    /**
     * Get random emoji from the list
     *
     * @return String
     */
    private String getRandomEmoji() {
        String[] emojis = {
            "hamburger", "pizza", "hotdog", "pancakes", "taco", "sandwich", "fries", "fried_egg",
            "shallow_pan_of_food", "stew", "green_salad", "bento", "rice", "spaghetti", "sushi", "fried_shrimp",
            "takeout_box", "meat_on_bone", "poultry_leg"
        };

        return ":" + emojis[new Random().nextInt(emojis.length)] + ":";
    }
}
